package gateway.wrb.model;

import lombok.Data;

@Data
public class RV001DTO {
    private String fbkname;
    private String msgdscd;
    private String trndt; // transac date
    private String trntm; //transac time
    private String msgno; // identify code
    private String wdracno; // withdaw acc no
    private String wdrviracno; //wthdraw virtual acc no
    private String rcvacno; // reiceive acc no (stk nop tien)
    private String rcvviracno; // receive vir acc no (stk ao nop tien)
    private String rcvacdppenm; // name of receive (ten nguoi)
    private String curcd;
    private String wdram;
    private String tobkdscd;
    private String istdscd;
    private String incdaccgb;
    private String rcvbk1cd;
    private String rcvbk2cd;
    private String regmodcd;
    private String trnstat;
    private String trnsrno;
    private String refno;
    private String vractcusnm;
    private String stsdscd;
    private String msgdscde;
    private String nortrancnte;
    private String nortrantotamte;
    private String orgcantrancnte;
    private String orgcantrantotamte;
    private String cantrancnte;
    private String cantrantotamte;

    public RV001DTO() {
    }

    public RV001DTO(String fbkname, String msgdscd, String trndt, String trntm, String msgno, String wdracno, String wdrviracno, String rcvacno, String rcvviracno, String rcvacdppenm, String curcd, String wdram, String tobkdscd, String istdscd, String incdaccgb, String rcvbk1cd, String rcvbk2cd, String regmodcd, String trnstat, String trnsrno, String refno, String vractcusnm, String stsdscd, String msgdscde, String nortrancnte, String nortrantotamte, String orgcantrancnte, String orgcantrantotamte, String cantrancnte, String cantrantotamte) {
        this.fbkname = fbkname;
        this.msgdscd = msgdscd;
        this.trndt = trndt;
        this.trntm = trntm;
        this.msgno = msgno;
        this.wdracno = wdracno;
        this.wdrviracno = wdrviracno;
        this.rcvacno = rcvacno;
        this.rcvviracno = rcvviracno;
        this.rcvacdppenm = rcvacdppenm;
        this.curcd = curcd;
        this.wdram = wdram;
        this.tobkdscd = tobkdscd;
        this.istdscd = istdscd;
        this.incdaccgb = incdaccgb;
        this.rcvbk1cd = rcvbk1cd;
        this.rcvbk2cd = rcvbk2cd;
        this.regmodcd = regmodcd;
        this.trnstat = trnstat;
        this.trnsrno = trnsrno;
        this.refno = refno;
        this.vractcusnm = vractcusnm;
        this.stsdscd = stsdscd;
        this.msgdscde = msgdscde;
        this.nortrancnte = nortrancnte;
        this.nortrantotamte = nortrantotamte;
        this.orgcantrancnte = orgcantrancnte;
        this.orgcantrantotamte = orgcantrantotamte;
        this.cantrancnte = cantrancnte;
        this.cantrantotamte = cantrantotamte;
    }
}