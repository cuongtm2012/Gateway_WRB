package gateway.wrb.model;

import lombok.Data;

@Data
public class HT002DTO {

    private String actNo;
    private String trnDt;
    private String trnTm;
    private String drCr;
    private String trnAmt;
    private String trnAfBl;
    private String brCd;
    private String chkAmt;
    private String trnType;
    private String particular;
    private String deqSeq;
    private String status;
    private String channelType;
    private String trnSrno;
    private String destAccount;
    private String recieveName;
    private String refTxt;
    private String depRmk;
    private String trmPrcSrno;
    private String virActNo;

    public HT002DTO() {
    }

    public HT002DTO(String actNo, String trnDt, String trnTm, String drCr, String trnAmt, String trnAfBl, String brCd, String chkAmt, String trnType, String particular, String deqSeq, String status, String channelType, String trnSrno, String destAccount, String recieveName, String refTxt, String depRmk, String trmPrcSrno, String virActNo) {
        this.actNo = actNo;
        this.trnDt = trnDt;
        this.trnTm = trnTm;
        this.drCr = drCr;
        this.trnAmt = trnAmt;
        this.trnAfBl = trnAfBl;
        this.brCd = brCd;
        this.chkAmt = chkAmt;
        this.trnType = trnType;
        this.particular = particular;
        this.deqSeq = deqSeq;
        this.status = status;
        this.channelType = channelType;
        this.trnSrno = trnSrno;
        this.destAccount = destAccount;
        this.recieveName = recieveName;
        this.refTxt = refTxt;
        this.depRmk = depRmk;
        this.trmPrcSrno = trmPrcSrno;
        this.virActNo = virActNo;
    }
}